<%@page contentType="text/html" pageEncoding="UTF-8" session="true" import="java.util.*, people.*, count.* "%>
<jsp:useBean id="peopleList" class="people.PeopleList" scope="application"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Список пользователей</h1>

        <table border="1" cellspacing="0" cellpadding="2">
<%
            for (People p : peopleList.values()) {
%>
                <tr><td><%= p.getId() %></td><td><%= p.getFIO() %></td><td><%= p.getLogin() %></td></tr>
<%
            }
%>
        </table>

    </body>
</html>

