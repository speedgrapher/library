/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

import java.util.*;


public class PeopleList {
    private Map <String,People> hm=null;
    private void load() {
        People p = new People ("Евсин","evsin"); this.add(p);
               p = new People ("Валеева","valeeva"); this.add(p);
               p = new People ("xxx","yyy"); this.add(p);
    }
    
    public PeopleList(){
        hm = new LinkedHashMap <String,People>();
        this.load();
    }
    public Collection<People> values() {
        return hm.values();
    }
            
            
    public void add (People p) { hm.put(p.getLogin(),p); }
    
}
