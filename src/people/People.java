package people;

import java.util.Date;

public class People {
    
    private String fio= "";
    
    private Integer id;
    private String name = "";
    private String surname = "";
    private String login = "";
    private Date birthday;
    private static int counter = 1;
    
    public void set_name(String name_){
	this.name=name_;
    }

    public void set_surname(String surname_){
	this.surname=surname_;
    }

    public void set_birthday(Date birthday_){
	this.birthday=birthday_;
    }
    
    public void setFIO(String fio_){
	this.fio=fio_;
    }

    public String getFIO(){
	return this.fio;
    }

    public void setLogin(String login_){
	this.login=login_;
    }

    public String getLogin(){
	return this.login;
    }
    

    public int getId(){
	return this.id;
    }
    
    public People(){
	fio="";
        login="";
	id=counter++;
    }

    public People(String fio_, String login_){
	this.fio=fio_;
        this.login=login_;
	this.id=counter++;
    }

    public String toString(){
	return "fio=="+fio+"id=="+id+"login=="+login;
    }
}

